#ifndef STRIPE_H
#define STRIPE_H

#include "comm.h"

// stripe data
void store(const Comm& comm, void* data, int scnt);

// 
void load(const Comm& comm, void* data, int scnt);

#endif
