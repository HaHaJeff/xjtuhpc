#ifndef CHECKPOINT_H
#define CHECKPOINT_H

#include "constant.h"
#include "field.h"
#include "refcount.h"
#include "IOStrategy/POSIXIO.h"
#include "IOStrategy/MPIIO.h"
#include "shm.h"
#include <mpi.h>

// TODO: 把数据直接交给checkpoint对象，由checkpoint完成数据的恢复与存储
// 采用double***对三维数据进行存储，那么是不是需要考虑数据的拷贝，是否可以考虑引入引用计数技术

// KeyData用于保存关键数据
// pnew用于这一步的检查点数据保存
// 完成之后将pnew copy到pold
// pvalid用于判断pold是否有效
// 如果pvalid=*，说明pold的checkpoint有效，如果pvalid=#，说明pnew的checkpoint有效，不需要拷贝
struct KeyData {
	KeyData(int);
	void store(double*, size_t);
	double* load(size_t);
	void storeCst(double);
	double loadCst();
	// init memory
	void init(size_t, int, double*);
	void exchange();
	void removeShm();

	void start();
	void end();
	void setValid(char*);

	bool old_valid() { return strcmp(pvalid, "*") == 0; }
	bool new_valid() { return strcmp(pvalid, "#") == 0; }
	bool valid() { return old_valid() || new_valid(); }

	SharedMem shm;
	// pnew means self data, in shared memory
	// however, the valid memory of pnew must expend a stripe size
	double* pnew;
	// pold means last valid data, in shared memory
	double* pold; 
	double* pcur;
	char* pvalid;
	size_t ssize;
	size_t size;

	void printInfo();
};

struct Base {
	// rank means a magic number for shared memory id
	Base(int magic);
	// 数据大小，条带个数，原始空间（原始控制应该需要多分配一个stripe大小的空间）
	void init(size_t, int, double*);
	void storeCst(double);
	void store(double*, size_t);
	double* load(size_t);
	double loadCst();
	void exchange();
	void removeShm();
	
	void start();
	void end();
	bool valid() { return key_.valid(); }
	bool old_valid() { return key_.old_valid(); }
	bool new_valid() { return key_.new_valid(); }

	double* getNew() { return key_.pnew; }
	double* getOld() { return key_.pold; } 

	KeyData key_;   // for key point data
};


class Level1 : public Base{
public:
	Level1(int);
};

class Level2 : public Base{
public:
	Level2(MPI_Comm, int, int, int);	
	
	void generate_raid5();
	void restore_raid5(int lost_rank);
	void print();

	MPI_Comm comm_; 	// for comunication, already split into process
	int stripe_;     	// for stripe
	int rank_;      	// for rank
};

struct LostInfo {
	int lost_rank;
	int lost_num;
};

LostInfo ErrorDetect(Level2& l2);

class Level3 {
public:
	Level3(Strategy* io) : io_(io) {}
	int write(double* ptr, int count, int rank, int group, MPI_Comm& comm);
	int read(double* ptr, int count, int rank, int group, MPI_Comm& comm);
	int exist();

private:
	Strategy* io_;
};

// 1. Level1 l1(comm, group, rank);
// 2. l1.init(size, stripe);   size for bytes
// 3. l1.start();
// 4. l1.store(ptr, size);
// 5. ...
// 6. l1.storeCst(d);
// 7. ...
// 8. l1.end();

class Checkpoint {
public:
  //Constant: int* size, Field: double*, vector<int>(x,y,z,k,block_id)
  Checkpoint(RCConstant& constant, RCField& field, int group);
  void RestoreConstant(Strategy& io);
  void RestoreField(Strategy& io);
  void SaveConstant(Strategy& io);
  void SaveField(Strategy& io);
  const Constant& GetConstant();
  const Field& GetField();

private:
  RCConstant constant_;
  RCField    field_;
  int group_;
};

void SetCheckpoint(RCConstant& constant, RCField& field, int group);
void Restart(RCConstant& constant, RCField& field, int group);

void SetCheckpoint(RCConstant& constant, RCField& field, MPI_Comm comm, int rank, int group);
void Restart(RCConstant& constant, RCField& field, MPI_Comm, int rank, int group);

#endif
