#ifndef COMM_H
#define COMM_H

#include <mpi.h>

// hold info of communication
class Comm {
public:
	explicit Comm(MPI_Comm, int, int);
	void allReduce(const void* src, void* dst, int cnt, MPI_Datatype, MPI_Op op);
	void reduce(const void* src, void* dst, int cnt, MPI_Datatype, MPI_Op op, int root);
private:
	MPI_Comm comm_;
	int rank_;
	int size_;
};

#endif
