#ifndef CKCONFIGURE_H
#define CKCONFIGURE_H

#include "tinyxml.h"

class CKConfigure {
public:
    // tinyxml loadfile
    CKConfigure(const char* filename);
    
    // generate empty xml file(filename is "CKConfigure.xml")
    // frequent, group_size, root_dir
    bool Init();

	// Checkpoint period
    int GetPeriod();
	// Process group size
    int GetProcessGroupSize();
	// IO group size
	int GetIOGroupSize();
	// Reading/Writing file paraments 
	int* GetBlockSize();
	// root directory of CR files
    const char* GetRootDir();
	// Doing checkpoint flag
	int GetDoCK();
	// Recovering flag
	int GetReadCK();
	// Level1 period
	int GetLevel1();
	// Level2 period
	int GetLevel2();
	// Level3 period
	int GetLevel3();
	// Nodes of application;
	int GetNodes();
	// Proces of node
	int GetProcs();

    ~CKConfigure();

private:
    TiXmlDocument* config_;
    TiXmlElement*  root_; 
    TiXmlElement* properties_;
};

#endif
