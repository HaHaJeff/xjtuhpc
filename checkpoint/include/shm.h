#ifndef SHM_H
#define SHM_H

#include <sys/types.h>

#define ALIGN(x, mask) (x+mask-1)&~(mask-1)

class SMInfo {
public:
	static unsigned long int getShmall();
	static int setShmall(unsigned long int value);
	static unsigned long int getUsed();
};

class SharedMem {
public:
	SharedMem(int);
	~SharedMem();
	// size means orign data size
	// stripe means for numbers of nodes in the group
	// malloc will automatic expand
	// total size = ceil[size/(stripe-1)]*stripe
	void* malloc(size_t size, int stripe);
	void* malloc(size_t size);
	void free(void*);	
	void remove();
	void print();
private:
	key_t hash(int id);
	size_t align(size_t size);
private:
	static const key_t MAGIC = 0xFC000000;
	static const size_t PAGESIZE = 4096;	
	key_t key_;
	int id_;
	void *ptr_;
};

#endif

/*
unsigned long int getUsed() {
	struct shm_info shminf;
	if (shmctl(0, SHM_INFO, (struct shmid_ds*)&shminf) < 0) {
		return -1;

	}
	// page
	return shminf.shm_tot;
}

unsigned long int getShmall() {
	FILE* f = nullptr;
	unsigned long value = 0;
	char buf[512];
	if ((f = fopen("/proc/sys/kernel/shmall", "r")) != nullptr) {
		if (fgets(buf, sizeof(buf), f) != nullptr) value = strtoul(buf, NULL, 10);
		fclose(f);

	}

	return value;
}

int setShmall(unsigned long int value) {
	FILE* f = nullptr;
	char buf[512];
	int retval = 0;
	unsigned long int need = value + getUsed();
	if ((f = fopen("/proc/sys/kernel/shmall", "w")) != nullptr) {
		if (snprintf(buf, sizeof(buf), "%lu\n", need) >= sizeof(buf) || fwrite(buf, 1, strlen(buf), f) != strlen(buf)) retval = -1;
		fclose(f);

	} else {
		retval = -1;

	}
	return retval;
}

void print() {
	int shmused = getUsed();
	printf("original shmall: %lu pages\n", getShmall());
	printf("shmused: %lu pages (%lu bytes)\n", shmused, shmused*getpagesize());

}

void* SMMalloc(key_t key, size_t size) {
	int shmid = shmget(key, size, IPC_CREAT | 0666);
	if (shmid == -1) {
		perror("shmget");

	}
	void *ptr = shmat(shmid, NULL, 0);
	if (ptr == (void*)-1) {
		perror("shmat");

	}
	return ptr;

}

int SMDetach(const void* shmaddr) {
	return shmdt(shmaddr);
}
*/
