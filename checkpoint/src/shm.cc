#include "shm.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

unsigned long int SMInfo::getShmall() {
	FILE* f = nullptr;
	unsigned long value = 0;
	char buf[512];
	if ((f = fopen("/proc/sys/kernel/shmall", "r")) != nullptr) {
		if (fgets(buf, sizeof(buf), f) != nullptr) value = strtoul(buf, NULL, 10);
		fclose(f);

	}

	return value;
}

int SMInfo::setShmall(unsigned long int value) {
	FILE* f = nullptr;
	char buf[512];
	int retval = 0;
	unsigned long int need = value + getUsed();
	if ((f = fopen("/proc/sys/kernel/shmall", "w")) != nullptr) {
		if (snprintf(buf, sizeof(buf), "%lu\n", need) >= sizeof(buf) || fwrite(buf, 1, strlen(buf), f) != strlen(buf)) retval = -1;
		fclose(f);

	} else {
		retval = -1;

	}
	return retval;
}

unsigned long int SMInfo::getUsed() {
	struct shm_info shminf;
	if (shmctl(0, SHM_INFO, (struct shmid_ds*)&shminf) < 0) {
		return -1;

	}
	// page
	return shminf.shm_tot;
}

SharedMem::SharedMem(int k) : key_(hash(k)), id_(0), ptr_(nullptr) {}

SharedMem::~SharedMem() {
	if (nullptr != ptr_) {
		free(ptr_);
	}
}

void *SharedMem::malloc(size_t size, int stripe) {
	assert(stripe >= 1);
	if (stripe > 1) size = (size/(stripe-1) + 1) * stripe;
	size_t aligned = align(size);
	int shmid = shmget(key_, aligned, IPC_CREAT | 0660);
	if (shmid == -1) {
		if (errno == ENOENT)
		perror("shmget");
		return nullptr;
	}
	id_ = shmid;
	void* ptr = shmat(shmid, nullptr, 0);
	if (ptr == (void*)-1) {
		return nullptr;
	}
	ptr_ = ptr;
	return ptr;
}

void* SharedMem::malloc(size_t size) {
	size_t aligned = align(size);
	int	shmid = shmget(key_, aligned, IPC_CREAT | 0660);
		
	if (shmid == -1) {
		return nullptr;
	}
	id_ = shmid;
	void* ptr = shmat(shmid, nullptr, 0);
	if (ptr == (void*)-1) {
		return nullptr;
	}
	ptr_ = ptr;
	return ptr;
}

void SharedMem::free(void* ptr) {
	if (ptr != nullptr) {
		shmdt(ptr);
	}
	ptr_ = ptr = nullptr;
}

void SharedMem::remove() {
	if (id_ != 0) {
		shmctl(id_, IPC_RMID, nullptr);
	} 
}

void SharedMem::print() {
	int orign   = SMInfo::getShmall();
	int shmused = SMInfo::getUsed();
	const int MB = 1024*1024;
	printf("original shmall: %lu pages (%lu MB)\n", orign, orign*getpagesize()/MB);
	printf("shmused: %lu pages (%lu MB)\n", shmused, shmused*getpagesize()/MB);
}

key_t SharedMem::hash(int k) {
	return k+MAGIC;
}

size_t SharedMem::align(size_t size) {
	return (size+PAGESIZE-1)&~(PAGESIZE-1);
}
