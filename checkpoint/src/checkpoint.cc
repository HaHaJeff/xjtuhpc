#include "checkpoint.h"
#include "IOStrategy/Data.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <assert.h>

static int gcd(int a, int b)
{
	return b == 0 ? a : gcd(b, a%b);
}

static int lcm(int a, int b)
{
	return a/gcd(a, b)*b;
}

Checkpoint::Checkpoint(RCConstant& constant, RCField& field, int group) :
	constant_(constant),
	field_(field), group_(group) 
{}

const Constant& Checkpoint::GetConstant() {
	return *constant_;
}

const Field& Checkpoint::GetField() {
	return *field_;
}

Base::Base(int rank) : key_(rank) {
}

void Base::store(double* ptr, size_t size) {
	key_.store(ptr, size);
}

double* Base::load(size_t size) {
	return key_.load(size);
}

void Base::init(size_t size, int stripe, double* ptr) {
	key_.init(size, stripe, ptr);	
}

void Base::start() {
	key_.start();
} 

void Base::end() {
	key_.end();
}

void Base::removeShm()
{
	key_.removeShm();
}

void KeyData::printInfo()
{
	printf("pnew: %p\n", pnew);
	printf("pold: %p\n", pold);
	printf("pcur: %p\n", pcur);
}

KeyData::KeyData(int rank) : shm(rank){
}


// ptr需要多分配一个stripe大小的空间
void KeyData::init(size_t s, int stripe, double* ptr) {
	// 向上取整
	s /= sizeof(double);
	stripe -= 1;
	if (stripe > 0)
		ssize = (s + stripe - 1)/stripe * sizeof(double);
	else
		ssize = s * sizeof(double);
	stripe += 1;	
	size = ssize * stripe;
	char* m= reinterpret_cast<char*>(shm.malloc(ssize*stripe+64));
	assert(m != nullptr);
	pvalid = m;

	pold = reinterpret_cast<double*>(&m[1]);
	pcur = ptr;
	pnew =  ptr;
}

void KeyData::setValid(char* valid) {
	*pvalid = *valid;
}

void KeyData::store(double* ptr, size_t s) {
	memcpy(pcur, ptr, s);
	s /= sizeof(double);
	pcur += s;
}

void KeyData::storeCst(double d) {
	memcpy(pcur, &d, sizeof(d));
	pcur += 1;
}

double* KeyData::load(size_t s) {
	double* ret = pcur;
	s /= sizeof(double);
	pcur += s;
	return ret;
}

double KeyData::loadCst() {
	double* ret = pcur;
	pcur += 1;
	return *ret;
}

void KeyData::exchange() {
	memcpy(pold, pnew, size);
	pcur = pnew;
}

void KeyData::removeShm() {
	shm.remove();	
}

void KeyData::start() {
	setValid("#");
	pcur = pnew;
}

void KeyData::end() {
	exchange();
	setValid("*");
}

Level2::Level2(MPI_Comm comm, int stripe, int rank, int magic) : Base(magic), comm_(comm), stripe_(stripe), rank_(rank)
{
}


// MPI_Reduce(sendbuf, recvbuf, count, datatype, op, root, comm)
// 采用MPI_SUM可能会
void Level2::generate_raid5()
{
	// point checksum at last stripe of keydata
	double* ptr = &key_.pcur[0];
	size_t count = key_.ssize/sizeof(double);
	int bcnt = 16*1024;
	double* checksum = &key_.pcur[0] + (stripe_-1)*count;
	memset(checksum, 0, key_.ssize);
	for (size_t i = 0; i < stripe_; i++)
	{
		size_t d_stripe = i > rank_ ? i - 1 : i;
		if (i == rank_)
		{
			for (ssize_t left = count; left > 0; left -= bcnt)
			{
				size_t sz = left > bcnt ? bcnt : left;
				MPI_Reduce(MPI_IN_PLACE, checksum+count-left, sz, MPI_DOUBLE, MPI_SUM, i, comm_);
			}
		}
		else
		{
			for (ssize_t left = count; left > 0; left -= bcnt)
			{
				size_t sz = left > bcnt ? bcnt : left;
				MPI_Reduce(ptr+d_stripe*count+count-left, nullptr, sz, MPI_DOUBLE, MPI_SUM, i, comm_);
			}
		}
	}
}

// lost_rank由故障探测模块决定
void Level2::restore_raid5(int lost_rank)
{
	if (key_.old_valid())
	{
		memcpy(key_.pcur, key_.pold, key_.size);
	}
	double* ptr = &key_.pcur[0];

	//每次发送数据大小
	size_t count = key_.ssize/sizeof(double);
	size_t bcnt = 16*1024;
	double* checksum = &ptr[0] + (stripe_-1)*count;

	if (rank_ == lost_rank)
	{
		memset(ptr, 0, key_.size);

		for (size_t i = 0; i < stripe_; i++)
		{
			size_t d_stripe = i > rank_ ? i - 1 : i;
			if (i == rank_)
			{
				for (ssize_t left = count; left > 0; left -= bcnt)
				{
					size_t sz = left > bcnt ? bcnt : left;
					MPI_Reduce(MPI_IN_PLACE, checksum+count-left, sz, MPI_DOUBLE, MPI_SUM, lost_rank, comm_);
				}
			}
			else
			{
				for (ssize_t left = count; left > 0; left -= bcnt)
				{
					size_t sz = left > bcnt ? bcnt : left;
					MPI_Reduce(MPI_IN_PLACE, ptr+d_stripe*count+count-left, sz, MPI_DOUBLE, MPI_SUM, lost_rank, comm_);

				}
			}
		}
		for (size_t i = 0; i < stripe_-1; i++)
		{
			for (size_t j = 0; j < count; j++)
			{
				ptr[i*count+j] *= -1.0f;
			}
		}
	}
	else
	{
		for (size_t i = 0; i < count; i++)
		{
			checksum[i] *= -1.0f;
		}
		for (size_t i = 0; i < stripe_; i++)
		{
			size_t d_stripe = i > rank_ ? i - 1 : i;
			if (i == rank_)
			{
				for (ssize_t left = count; left > 0; left -= bcnt)
				{
					size_t sz = left > bcnt ? bcnt : left;
					MPI_Reduce(checksum+count-left, nullptr, sz, MPI_DOUBLE, MPI_SUM, lost_rank, comm_);
				}
			}
			else
			{
				for (ssize_t left = count; left > 0; left -= bcnt)
				{
					size_t sz = left > bcnt ? bcnt : left;
					MPI_Reduce(ptr+d_stripe*count+count-left, nullptr, sz, MPI_DOUBLE, MPI_SUM, lost_rank, comm_);
				}
			}
		}
		for (size_t i = 0; i < count; i++)
		{
			checksum[i] *= -1.0f;
		}
	}
}

void Level2::print()
{
	if (rank_ == 0)
	{
		for (size_t i = 0; i < key_.ssize*stripe_/sizeof(double); i++)
		{
			printf("%lf ", key_.pcur[i]);
		}
		printf("\n");

		printf("key info\n");
		key_.printInfo();
	}
}

Level1::Level1(int magic) : Base(magic) 
{
}

LostInfo ErrorDetect(Level2& l2) {
	MPI_Comm comm = l2.comm_;
	int rank = l2.rank_;
	int has_lost = 0;
	int lost_rank = -1;

	if (!l2.valid())
	{
		lost_rank = rank;
		has_lost = 1;
	}
	MPI_Allreduce(MPI_IN_PLACE, &has_lost, 1, MPI_INT, MPI_SUM, comm);
	MPI_Allreduce(MPI_IN_PLACE, &lost_rank, 1, MPI_INT, MPI_MAX, comm);
	return {lost_rank, has_lost};
}


// before write or read, need set file view
void Checkpoint::RestoreConstant(Strategy& io) {
	int block_id = field_.GetInfo()[4];
	int count = constant_.GetSize();
	double* cst = nullptr;
	Malloc(cst, count);
	Data_3D data(cst, count, block_id, "");
	io.Lseek(block_id%group_*count*sizeof(double));
	io.Read(data);
	TConstant& constant = constant_.GetConstant();
	for (int i = 0; i < count; i++) {
		constant[i] = static_cast<int>(cst[i]);
	}

	Free(cst);
	cst = nullptr;
}

void Checkpoint::RestoreField(Strategy& io) {
	std::vector<int> info = field_.GetInfo();
	int num = info[0];
	int x = info[1];
	int y = info[2];
	int z = info[3];
	int block_id = info[4];
	int count = num * x * y * z;
	double* field = field_.GetField();
	Data_3D data(field, num*x*y*z, block_id, "");
	io.Lseek(block_id%group_*count*sizeof(double));
	io.Read(data);
}

void Checkpoint::SaveConstant(Strategy& io) {
	int block_id = field_.GetInfo()[4];
	int count = constant_.GetSize();
	double* cst = new double[count];
	TConstant constant = constant_.GetConstant();

	for (int i = 0; i < count; i++) {
		cst[i] = static_cast<double>(constant[i]);
	}

	Data_3D data(cst, count, block_id, "");
	io.Lseek(block_id%group_*count*sizeof(double));
	io.Write(data);
	delete[] cst;
	cst = nullptr;
}

void Checkpoint::SaveField(Strategy& io) {
	std::vector<int> info = field_.GetInfo();
	int num = info[0];
	int x = info[1];
	int y = info[2];
	int z = info[3];
	int block_id = info[4];
	int count = num * x * y * z;
	double* field = field_.GetField();
	Data_3D data(field, num*x* y* z, block_id, "");
	io.Lseek(block_id%group_*count*sizeof(double));
	io.Write(data);
}

void SetCheckpoint(RCConstant& constant, RCField& field, int group) {
	Checkpoint ck(constant, field, group);
	POSIXIO io;
	Strategy* strategy = io.GetIOStrategy(static_cast<TYPE>(0));
	char ckfld[12];
	char ckcst[15];
	int block_id = field.GetInfo()[4]/group;

	char tmpfld[15];
	char tmpcst[18];

	snprintf(tmpfld, sizeof(tmpfld), "tmp_field_%02d", block_id);
	snprintf(tmpcst, sizeof(tmpcst), "tmp_constant_%02d", block_id);

	// save field information
	snprintf(ckfld, sizeof(ckfld), "ck_field_%02d", block_id);
	strategy->Open(tmpfld);
	ck.SaveField(*strategy);
	strategy->Close();

	// save constant information
	snprintf(ckcst, sizeof(ckcst), "ck_constant_%02d", block_id);
	strategy->Open(tmpcst);
	ck.SaveConstant(*strategy);
	strategy->Close();

	delete strategy;

	rename(tmpfld, ckfld);
	rename(tmpcst, ckcst);
}

int Level3::write(double* ptr, int count, int rank, int group, MPI_Comm& comm)
{

	char tmp_filename[30];
	snprintf(tmp_filename, sizeof(tmp_filename), ".ck_data/tmp_file_%02d", rank/group);

	off_t offset = rank % group * count * sizeof(double);
	Data_3D data(ptr, count, rank, "");

	io_->Open(tmp_filename);
	io_->Lseek(offset);
	io_->Write(data);
	io_->Close();

	char filename[30];
	snprintf(filename, sizeof(filename), ".ck_data/file_%02d", rank/group);
	rename(tmp_filename, filename);

	return 0;
}

int Level3::read(double* ptr, int count, int rank, int group, MPI_Comm& comm)
{

	if (exist() == 0) {
		return -1;	
	}
	char filename[30];
	snprintf(filename, sizeof(filename), ".ck_data/file_%02d", rank/group);

	off_t offset = rank % group * count * sizeof(double);

	Data_3D data(ptr, count, rank, "");

	io_->Open(filename);
	io_->Lseek(offset);
	io_->Read(data);
	io_->Close();
	return 0;
}

int Level3::exist() {
	DIR *dir = opendir(".ck_data/");
	struct dirent *ent;
	if (dir == NULL)
	{  
		perror("seekkey.c-98-opendir");
		return -1;
	}  
	while (1)
	{  
		ent = readdir (dir);
		if (ent <= 0)
		{  
			break;
		}  
		if ((strcmp(".", ent->d_name)==0) || (strcmp("..", ent->d_name)==0))
		{  
			continue;
		}  
		if ((ent->d_type == 4) || (ent->d_type == 8))
		{  
			return 1;
		}  
	}  
	return 0;
}

void Restart(RCConstant& constant, RCField& field, int group) {
	Checkpoint ck(constant, field, group);
	POSIXIO io;
	Strategy* strategy = io.GetIOStrategy(static_cast<TYPE>(0));
	char ckfld[12];
	char ckcst[15];
	int block_id = field.GetInfo()[4]/group;

	// save field information
	snprintf(ckfld, sizeof(ckfld), "ck_field_%02d", block_id);
	strategy->Open(ckfld);
	ck.RestoreField(*strategy);
	strategy->Close();

	// save constant information
	snprintf(ckcst, sizeof(ckcst), "ck_constant_%02d", block_id);
	strategy->Open(ckcst);
	ck.RestoreConstant(*strategy);
	strategy->Close();

	delete strategy;
}

void SetCheckpoint(RCConstant& constant, RCField& field, MPI_Comm comm, int rank, int group) {
	Checkpoint ck(constant, field, group);
	MPIIO io(comm, rank);
	Strategy* strategy = io.GetIOStrategy(static_cast<TYPE>(0));
	char ckfld[12];
	char ckcst[15];
	int block_id = field.GetInfo()[4]/group;

	char tmpfld[15];
	char tmpcst[18];

	snprintf(tmpfld, sizeof(tmpfld), "tmp_field_%02d", block_id);
	snprintf(tmpcst, sizeof(tmpcst), "tmp_constant_%02d", block_id);

	// save field information
	snprintf(ckfld, sizeof(ckfld), "ck_field_%02d", block_id);
	strategy->Open(tmpfld);
	ck.SaveField(*strategy);
	if (rank == 0) {
		printf("after saveField\n");
	}
	strategy->Close();

	// save constant information
	snprintf(ckcst, sizeof(ckcst), "ck_constant_%02d", block_id);
	strategy->Open(tmpcst);
	ck.SaveConstant(*strategy);
	strategy->Close();

	delete strategy;

	rename(tmpfld, ckfld);
	rename(tmpcst, ckcst);
}

void Restart(RCConstant& constant, RCField& field, MPI_Comm comm, int rank, int group) {
	Checkpoint ck(constant, field, group);
	MPIIO io(comm, rank);
	Strategy* strategy = io.GetIOStrategy(static_cast<TYPE>(0));
	char ckfld[12];
	char ckcst[15];
	int block_id = field.GetInfo()[4]/group;

	// save field information
	snprintf(ckfld, sizeof(ckfld), "ck_field_%02d", block_id);
	strategy->Open(ckfld);
	ck.RestoreField(*strategy);
	strategy->Close();

	// save constant information
	snprintf(ckcst, sizeof(ckcst), "ck_constant_%02d", block_id);
	strategy->Open(ckcst);
	ck.RestoreConstant(*strategy);
	strategy->Close();

	delete strategy;
}
