#include "ckconfigure.h"
#include <cstdlib>
#include <stdio.h>
#include <iostream>

CKConfigure::CKConfigure(const char* filename) : config_(new TiXmlDocument()) {
  if (!config_->LoadFile(filename)) {
    std::cerr << config_->ErrorDesc() << std::endl;
    exit(-1);
  }
  if ((root_ = config_->RootElement()) == NULL) {
    std::cerr << "Failed to load file: No root element." << std::endl;
    exit(-1);
  }
  properties_ = root_->FirstChildElement();
}

CKConfigure::~CKConfigure() {
    if (config_ != NULL) delete config_;

    config_ = NULL;
}


bool CKConfigure::Init() {

}

int CKConfigure::GetPeriod() {
    return atoi(properties_->Attribute("period"));
}

int CKConfigure::GetProcessGroupSize() {
    return atoi(properties_->Attribute("process_group"));
}

int CKConfigure::GetIOGroupSize() {
	return atoi(properties_->Attribute("io_group"));
}

int* CKConfigure::GetBlockSize() {
    int x = atoi(properties_->Attribute("block_x"));
    int y = atoi(properties_->Attribute("block_y"));
    int z = atoi(properties_->Attribute("block_z"));
    int* ret = (int*)malloc(3*sizeof(int));
    ret[0] = x;
    ret[1] = y;
    ret[2] = z;
    return ret;
}

const char* CKConfigure::GetRootDir() {
	const char* ret = properties_->Attribute("root_dir");
	return ret;
}

int CKConfigure::GetDoCK() {
	int r = atoi(properties_->Attribute("do_ck"));
	return r;
}

int CKConfigure::GetReadCK() {
	int r = atoi(properties_->Attribute("read_ck"));
	return r;
}

int CKConfigure::GetLevel1() {
	int r = atoi(properties_->Attribute("level1"));
	return r;
}

int CKConfigure::GetLevel2() {
	int r = atoi(properties_->Attribute("level2"));
	return r;
}

int CKConfigure::GetLevel3() {
	int r = atoi(properties_->Attribute("level3"));
	return r;
}

int CKConfigure::GetNodes() {
	int r = atoi(properties_->Attribute("nodes"));
	return r;
}

int CKConfigure::GetProcs() {
	int r = atoi(properties_->Attribute("procs"));
	return r;
}
