#include "comm.h"

Comm::Comm(MPI_Comm comm, int rank, int size): comm_(comm), rank_(rank), size_(size) {
}

void Comm::allReduce(const void* src, void* dst, int cnt, MPI_Datatype type, MPI_Op op) {
	MPI_Allreduce(src, dst, cnt, type, op, comm_);
}

void Comm::reduce(const void* src, void* dst, int cnt, MPI_Datatype type, MPI_Op op, int root) {
	MPI_Reduce(src, dst, cnt, type, op, root, comm_);
}
