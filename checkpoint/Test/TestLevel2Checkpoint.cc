#include <mpi.h>
#include <stdio.h>
#include "checkpoint.h"

void print(double* ptr, int size)
{
	int cnt = 3;
	for (int i = 0; i < size; i++)
	{
		printf("%lf ", ptr[i]);
		if (--cnt == 0)
		{
			cnt = 3;
			printf("\n");
		}
	}
}

int main(int argc, char** argv)
{
	int rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm comm;
	MPI_Comm_split(MPI_COMM_WORLD, rank/4, rank%4, &comm);

	int newRank = 0;
	MPI_Comm_rank(comm, &newRank);
	SharedMem shm(rank + 100);
	int size = 100*8;
	double* arr = static_cast<double*>(shm.malloc(2*size));

	if (rank == 0)
		print(arr, size/8);

	for (int i = 0; i < size/8; i++)
	{
		arr[i] = i+(newRank+1)*1+1;
	}

	if (rank == 0)
	{
		print(arr, size/8);
		printf("begin\n");
	}

	Level2 l2(comm, 4, newRank, rank);
	l2.init(size, 4, arr);
	l2.start();
	l2.generate_raid5();
	l2.end();

	int lost_rank = 0;
	
	double* ptr = l2.getOld();

	if (newRank == lost_rank)
	{
		memset(ptr, 0, size);
		memset(arr, 0, size);
	}
	l2.restore_raid5(lost_rank);
	l2.print();

	MPI_Finalize();
}
