#include "checkpoint.h"
#include "memory.h"
#include <cassert>

#include <iostream>
#include <string.h>
#include <thread>

int main(int argc, char**argv)
{
	int rank = 0, procs = 0;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	//MPIIO io(MPI_COMM_WORLD, rank);
	POSIXIO io;
	Strategy* strategy = io.GetIOStrategy(static_cast<TYPE>(0));
	Level3 l3(strategy);
	
	int size = 100000;
	double* ptr = static_cast<double*>(malloc(size*8));
	
	MPI_Comm comm = MPI_COMM_WORLD;

	l3.read(ptr, size, rank, procs, comm);

	for (int i = 10000; i < 13000; i++)
	{
		printf("%lf\n", ptr[i]);
	}
}
