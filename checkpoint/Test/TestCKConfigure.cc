#include "ckconfigure.h"
#include <iostream>

int main()
{
    CKConfigure config("checkpoint.xml");
	int process_group = config.GetProcessGroupSize();
	int io_group = config.GetIOGroupSize();
	int period = config.GetPeriod();
	int do_ck = config.GetDoCK();
	int read_ck = config.GetReadCK();
	int level1 = config.GetLevel1();
	int level2 = config.GetLevel2();
	int level3 = config.GetLevel3();
    std::cout << config.GetPeriod() << std::endl;
    std::cout << config.GetProcessGroupSize() << std::endl;
    int* num = config.GetBlockSize();
    std::cout << num[0] << "\t" << num[1] << "\t" << num[2] << std::endl;
    free(num);
}

