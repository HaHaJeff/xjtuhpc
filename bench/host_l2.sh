#!/bin/sh
# -N --nodes
# -n --ntasks   default cores of per task is one
# -c --cpus-per-task
#yhrun -N 150 -n 3600 --ntasks-per-node=1 -p th_mt1 ./rmshm.sh
# bench quant procs nodes group

#level2
#quant_arr=(100 200)
#procs_arr=(4 8 16 32 64 128 256 512)
#nodes_arr=(4 8 16 16 32 64)
#group_arr=(1 2 4 8)

#level3
#quant_arr=(100)
#procs_arr=(1600)
#nodes_arr=(200)
#group_arr=(1 4 8 16 32 64)

#debug
quant_arr=(50)
#procs_arr=(1440)
#nodes_arr=(120)
#group_arr=(1 2 3 4 5 6 8 10 12 15 20 24 30 40 60)
procs_arr=(320)
nodes_arr=(16)
group_arr=(1)

node_max=$2
task_per_node_limit=20
plat=$1
yhrun -N $node_max -n $node_max --ntasks-per-node=1 -p $plat ./rmshm.sh
echo "***********************************plagform{${plat}}***************************************"
for q in "${quant_arr[@]}"; do
	for p in "${procs_arr[@]}"; do
		for n in "${nodes_arr[@]}"; do
			task_per_node=`expr $p / $n`
			if [ $task_per_node -ne 20 ]
			then
				continue
			fi
			if [ $task_per_node -gt $task_per_node_limit ]
			then
				continue
			fi
			for g in "${group_arr[@]}"; do
				group_size=`expr $n / $g`
				if [ $group_size -ne 16 ]
				then
					continue
				fi
				printf "\n****quant: %d(MB)\tprocs: %d\tnodes: %d\tgroup: %d****\n" "$q" "$p" "$n" "$g"
				yhrun --checkpoint=1 --checkpoint-dir=./ -N $n -n $p --ntasks-per-node=$task_per_node -p $plat ./bench 2 1 $q $p $n $g
				yhrun -N $node_max -n $node_max --ntasks-per-node=1 -p $plat ./rmshm.sh
				printf "********************************************************************\n"
			done
		done
	done
done
yhrun -N $node_max -n $node_max --ntasks-per-node=1 -p $plat ./rmshm.sh
#yhrun -N 4 -n 4 --ntasks-per-node=1 -p $plat ./bench 200 4 4 1
#yhrun -N 4 -n 4 --ntasks-per-node=1 -p $plat ./rmshm.sh

