#include <mpi.h> 
#include <assert.h>
#include <cstdint>
#include <string>
#include <unistd.h>
#include "shm.h"
#include "checkpoint.h"

const size_t MB = 1024*1024;
int read_ck = 0;

typedef struct {
	unsigned long size,resident,share,text,lib,data,dt;
} statm_t;


void print(statm_t& result) {
	const int MB = 1024;
	unsigned long size = result.size/MB;
	unsigned long rss  = result.resident/MB;
	unsigned long share = result.share/MB;
	unsigned long test = result.text/MB;
	unsigned long lib = result.lib/MB;
	unsigned long data = result.data/MB;
	unsigned long dt = result.dt/MB;
	printf("memory usage:\n");
	printf("size:%ld rss:%ld share:%ld text:%ld lib:%ld data:%ld dt:%ld\n", size, rss, share, test, lib, data, dt);
	printf("\n");

}

void readMemoryStatus(statm_t& result)
{
	unsigned long dummy;
	const char* statm_path = "/proc/self/statm";

	FILE *f = fopen(statm_path,"r");
	if(!f){
		perror(statm_path);
		abort();
	}
	if(7 != fscanf(f,"%ld %ld %ld %ld %ld %ld %ld",
				&result.size,&result.resident,&result.share,&result.text,&result.lib,&result.data,&result.dt))
	{
		perror(statm_path);
		abort();
	}
	fclose(f);
}

enum CKTAG {
	LEVEL1 = 0,
	LEVEL2,
	LEVEL3
};

// split global communicator
// group means number of a node group
MPI_Comm Split(int procs, int nodes, int group)
{
	assert(procs%nodes == 0);
	assert(nodes%group == 0);

	int rank = -1;
	//process size in node group
	int node_group = procs/group;
	int per_proc_node = procs/nodes;

	// split to node group
	MPI_Comm comm = MPI_COMM_WORLD;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_split(comm, rank/node_group, rank%node_group, &comm);
	MPI_Comm_rank(comm, &rank);

	// split to proc group
	MPI_Comm_split(comm, rank%per_proc_node, rank/per_proc_node, &comm);

	return comm;
}

void init(double* ptr, int64_t size) {
	for (int i = 0; i < size; i++) {
		ptr[i] = i;
	}
}

size_t getFreeMem(){
	size_t freeMem = 0;	
	FILE* fp=popen("cat /proc/meminfo | grep MemFree:|sed -e 's/.*:[^0-9]//'","r");
	fscanf(fp,"%ld", &freeMem);
	return freeMem;
}

void bench_l1(int quant, int procs, int nodes, int group)
{
    // 测试数据占用字节数
	int64_t size = quant*MB;

	int rank = -1;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	size_t before = getFreeMem();
    
    // 共享内存初始化，每个进程指定不一样的标识符
	SharedMem shm(rank+0x02000000);
    // stripe表示进程组大小，同时也是编码的条带数
	int stripe = nodes/group;
    // 将输入数据的内存分配在共享内存区域
	double* ptr = static_cast<double*>(shm.malloc(size*sizeof(double), stripe));
	assert(ptr != nullptr);
    // 初始化输入数据
	init(ptr, size);
	
    // 进程分组
	MPI_Comm comm = Split(procs, nodes, group);
	int comm_rank = -1;
	MPI_Comm_rank(comm, &comm_rank);

	Level1 l1(rank);
	l1.init(size*sizeof(double), stripe, ptr);
	size_t after = 0;

    // 回滚代码
	if (read_ck) {
		double tStart = MPI_Wtime();
		l1.end();
		double tEnd = MPI_Wtime();
		if (rank == 0) {
			printf("recover from checkpoint level1 elapse time: %lf\n", tEnd - tStart);	
		}
	}

	for (int i = 0; i < 5; i++) {
		sleep(1);
		double tStart = MPI_Wtime();

        // 设置level1检查点
		l1.start();
		l1.end();

		double tEnd = MPI_Wtime();
		after = getFreeMem();

		if (rank == 0) {
			printf("%dth ckeckpoint, elapse time: %lf\n", i+1, tEnd - tStart);
		}
	}
	if (rank == 0) {
		printf("before(MB): %d,after(MB): %d,", before*1024/MB, after*1024/MB);
		printf("memory usage(MB):%ld\n", (before-after)*1024/MB);
	}

}

// group means number of node group
void bench_l2(int quant, int procs, int nodes, int group)
{
	size_t size = quant*MB;

	int rank = -1;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	size_t before = getFreeMem();
	SharedMem shm(rank+0x02000000);

	int stripe = nodes/group;

	double* ptr = static_cast<double*>(shm.malloc(size*sizeof(double), stripe));
	assert(ptr != nullptr);

	init(ptr, size);
	
	MPI_Comm comm = Split(procs, nodes, group);
	int comm_rank = -1;
	MPI_Comm_rank(comm, &comm_rank);

	Level2 l2(comm, stripe, comm_rank, rank);
	l2.init(size*sizeof(double), stripe, ptr);
	size_t after = 0;

	if (read_ck) {
		double tStart = MPI_Wtime();
        // level2需要编码对应的恢复算法，restore_raid5函数的参数为l丢失数据的进程号
		l2.restore_raid5(0);
		double tEnd = MPI_Wtime();
		if (rank == 0) {
			printf("recover from checkpoint level2 elapse time: %lf\n", tEnd - tStart);	
		}
	}

	for (int i = 0; i < 5; i++) {
		sleep(1);
		double tStart = MPI_Wtime();

        // 设置level2检查点
		l2.start();
        // 调用编码算法
		l2.generate_raid5();
		l2.end();

		double tEnd = MPI_Wtime();
		after = getFreeMem();

		if (rank == 0) {
			printf("%dth ckeckpoint, elapse time: %lf\n", i+1, tEnd - tStart);
		}
	}

	if (rank == 0) {
		printf("before(MB): %d,after(MB): %d,", before*1024/MB, after*1024/MB);
		printf("memory usage(MB):%ld\n", (before-after)*1024/MB);
	}

    double tStart = MPI_Wtime();
	for (int i = 0; i < size/8; i++) {
		ptr[i] += i;

	}
    double tEnd = MPI_Wtime();

    if (rank == 0) {
			printf("origin test: %lf\n", tEnd - tStart);	
    }
}

// group means number of io group 
void bench_l3(int quant, int procs, int nodes, int group)
{
	int64_t size = quant*MB;
	int rank = -1;
	MPI_Comm comm = MPI_COMM_WORLD;
	MPI_Comm_rank(comm, &rank);	
	size_t before = getFreeMem();
	SharedMem shm(rank+0x02000000);
	int stripe = 1;

	double* ptr = static_cast<double*>(shm.malloc(size*sizeof(double), stripe));
	assert(ptr != nullptr);
	init(ptr, size);
	
	int io_group_size = procs/group;	
	MPI_Comm_split(comm, rank/io_group_size, rank%io_group_size, &comm);
	
	MPIIO io(comm, rank);
	Strategy* strategy = io.GetIOStrategy(static_cast<TYPE>(0));
	Level3 l3(strategy);
	
	if (read_ck) {
		double tStart = MPI_Wtime();
		l3.read(ptr, size, rank, group, comm);
		double tEnd = MPI_Wtime();
		if (rank == 0) {
			printf("recover from checkpoint level3 elapse time: %lf\n", tEnd - tStart);	
		}
	}

	size_t after = 0;
	for (int i = 0; i < 3; i++) {
		sleep(1);
		MPI_Barrier(MPI_COMM_WORLD);
		double tStart = MPI_Wtime();
		l3.write(ptr, size, rank, group, comm);
		double tEnd = MPI_Wtime();
		after = getFreeMem();

		if (rank == 0) {
			printf("%dth ckeckpoint, elapse time: %lf\n", i+1, tEnd - tStart);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	if (rank == 0) {
		printf("before(MB): %d,after(MB): %d,", before*1024/MB, after*1024/MB);
		printf("memory usage(MB):%ld\n", (before-after)*1024/MB);
	}


}

void bench(CKTAG tag, int quant, int procs, int nodes, int group) {
	switch(tag) {
		case 1:
			bench_l1(quant, procs, nodes, group);
			break;
		case 2:
			bench_l2(quant, procs, nodes, group);
			break;
		case 3:
			bench_l3(quant, procs, nodes, group);
			break;
		default:
			break;
	}
}

int main(int argc, char** argv)
{
	MPI_Init(&argc, &argv);
	assert(argc == 7);
	CKTAG tag = static_cast<CKTAG>(std::stoi(argv[1]));
	read_ck = std::stoi(argv[2]);	
	int quant = std::stoi(argv[3]);
	int procs = std::stoi(argv[4]);
	int nodes = std::stoi(argv[5]);
	int group = std::stoi(argv[6]);

	bench(tag, quant, procs, nodes, group);
	const size_t GB = 1024 * 1024 * 1024;
	
	MPI_Finalize();
}
