**使用说明**
- bench.cc 用于性能测试w
- host_l1.sh 用于测试level1的时间开销
- host_l2.sh 用于测试level2的时间开销
- host_l3.sh 用于测试level3的时间开销
- run.sh 用于在天河二号或者天河三号上提交作业
- rmshm.sh 用于清除已分配节点上的共享内存
