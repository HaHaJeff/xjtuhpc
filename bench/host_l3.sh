#!/bin/sh
# -N --nodes
# -n --ntasks   default cores of per task is one
# -c --cpus-per-task
#yhrun -N 150 -n 3600 --ntasks-per-node=1 -p th_mt1 ./rmshm.sh
# bench quant procs nodes group

#level3
#quant_arr=(50)
#procs_arr=(2400)
#nodes_arr=(200)
#group_arr=(1 2 4 8 16 32 64 128 2400)

quant_arr=(25)
procs_arr=(240 480 720)
nodes_arr=(24 48 72)
group_arr=(24)

node_max=$2
#task_per_node_limit=12
task_per_node_limit=70
plat=$1
yhrun -N $node_max -n $node_max --ntasks-per-node=1 -p $plat ./rmshm.sh
echo "***********************************plagform{${plat}}***************************************"
for q in "${quant_arr[@]}"; do
	for p in "${procs_arr[@]}"; do
		for n in "${nodes_arr[@]}"; do
			task_per_node=`expr $p / $n`
			if [ $task_per_node -eq 0 ]
			then
				continue
			fi
			if [ $task_per_node -ne 10 ]
			then
				continue
			fi
			for g in "${group_arr[@]}"; do
				#if [ $g -ge $n ]
				#then
				#	continue
				#fi
				printf "\n****quant: %d(MB)\tprocs: %d\tnodes: %d\tgroup: %d****\n" "$q" "$p" "$n" "$g"
				yhrun -N $n -n $p --ntasks-per-node=$task_per_node --ntasks-per-core=1 -p $plat ./bench 3 0 $q $p $n $g
				yhrun -N $node_max -n $node_max --ntasks-per-node=1 -p $plat ./rmshm.sh
				yhrun -N 1 -n 1 --ntasks-per-node=1 -p $plat ./clear.sh
				printf "********************************************************************\n"
			done
		done
	done
done
yhrun -N $node_max -n $node_max --ntasks-per-node=1 -p $plat ./rmshm.sh
#yhrun -N 4 -n 4 --ntasks-per-node=1 -p $plat ./bench 200 4 4 1
#yhrun -N 4 -n 4 --ntasks-per-node=1 -p $plat ./rmshm.sh

